## Available Scripts

In the project directory, you can run:

> ***yarn install***

initialize the project with the dependencies.

> ***yarn start***

Runs the app in the development mode.
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

> ***yarn flow***
>
> Runs Flow type checker.



### ***Required node version: 17.3.0***
### ***Required yarn version: 1.22.17***
### ***Third party dependencies:***
- [Styled componentes](https://www.npmjs.com/package/styled-components)
- [React Suite](https://www.npmjs.com/package/rsuite)
- [Axios](https://www.npmjs.com/package/axios)
