// @flow
// Vendors
import * as React from "react";
// Styles
import "./App.css";
import "rsuite/dist/rsuite.min.css";
// Views
import AppView from "./app/app.view";

function App(): React.Node {
  return <AppView />;
}

export default App;
