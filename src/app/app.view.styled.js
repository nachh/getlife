// Vendors
import styled from "styled-components";
// Components
import { InputNumber } from "rsuite";

export const WrapperStyled = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  min-width: 700px;
`;

export const TableTitleStyled = styled.h4`
  margin-bottom: 5px;
  padding: 0;
`;

export const TableInputStyled = styled(InputNumber)`
  border: 1px solid black;
  border-radius: 0;
  margin-bottom: 10px;
  width: 200px;
`;

export const TableWrapperStyled = styled.div`
  margin-top: 10px;
  width: 700px;
`;
