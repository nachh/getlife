// @flow
// Vendors
import * as React from "react";
// Components
import { Table } from "rsuite";
// Styles
const styles = {
  border: "1px solid #575757",
  background: "#575757",
  color: "white",
};

const MasterTable = (props: Object): React.Node => {
  return (
    <Table
      style={{ border: "1px solid #575757" }}
      cellBordered
      bordered
      height={300}
      {...props}
    >
      <Table.Column width={60}>
        <Table.HeaderCell style={styles}>
          {props.config.COLUMN1.TITLE}
        </Table.HeaderCell>
        <Table.Cell dataKey={props.config.COLUMN1.dataKey} />
      </Table.Column>
      <Table.Column flexGrow={1}>
        <Table.HeaderCell style={styles}>
          {props.config.COLUMN2.TITLE}
        </Table.HeaderCell>
        <Table.Cell dataKey={props.config.COLUMN2.dataKey} />
      </Table.Column>
      <Table.Column flexGrow={2}>
        <Table.HeaderCell style={styles}>
          {props.config.COLUMN3.TITLE}
        </Table.HeaderCell>
        <Table.Cell dataKey={props.config.COLUMN3.dataKey} />
      </Table.Column>
    </Table>
  );
};

export default MasterTable;
