export default {
  TABLE1_PROPS: {
    COLUMN1: {
      dataKey: "nposts",
      TITLE: "#Posts",
    },
    COLUMN2: {
      dataKey: "name",
      TITLE: "Name",
    },
    COLUMN3: {
      dataKey: "email",
      TITLE: "Email",
    },
  },
  TABLE2_PROPS: {
    COLUMN1: {
      dataKey: "id",
      TITLE: "Id",
    },
    COLUMN2: {
      dataKey: "author",
      TITLE: "Author",
    },
    COLUMN3: {
      dataKey: "ncomments",
      TITLE: "# comments",
    },
  },
  TABLE3_PROPS: {
    COLUMN1: {
      dataKey: "id",
      TITLE: "Id",
    },
    COLUMN2: {
      dataKey: "author",
      TITLE: "Author",
    },
    COLUMN3: {
      dataKey: "nphotos",
      TITLE: "# Photos",
    },
  },
};
