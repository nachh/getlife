// @flow

export type viewHandlerTypes = {
  data: Object,
  doTask1: Function,
  doTask2: Function,
  doTask3: Function,
  setData: Function,
  setTask1: Function,
  setTask2: Function,
  setTask3: Function,
};

export type viewHandlerReturnTypes = {
  handleFetchAllData: Function,
  handleInitTask1: Function,
  handleInitTask2: Function,
  handleInitTask3: Function,
};
