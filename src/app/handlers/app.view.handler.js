// @flow
// Vendors
import axios from "axios";
// Constants
import constants from "./constants/app.view.handler.constants";
// Types
import type {
  viewHandlerTypes,
  viewHandlerReturnTypes,
} from "./types/app.view.handler.types";

const fetchAllDataHandler = (setData) => {
  axios
    .all(constants.REQUESTS)
    .then(
      axios.spread((...responses) => {
        const res = {};
        responses.map((response, i) => {
          res[constants.CATEGORIES[i]] = response.data;
        });
        setData(res);
      })
    )
    .catch((errors) => {
      return errors;
    });
};

const initHandlerTask1 = (data, doTask1, setTask1) => {
  setTask1(doTask1(data));
};

const initHandlerTask2 = (data, doTask2, setTask2, value) => {
  setTask2(doTask2(data, value));
};

const initHandlerTask3 = (data, doTask3, setTask3, value) => {
  setTask3(doTask3(data, value));
};

const AppViewHandler = ({
  data,
  doTask1,
  doTask2,
  doTask3,
  setData,
  setTask1,
  setTask2,
  setTask3,
}: viewHandlerTypes): viewHandlerReturnTypes => ({
  handleFetchAllData: () => fetchAllDataHandler(setData),
  handleInitTask1: () => initHandlerTask1(data, doTask1, setTask1),
  handleInitTask2: (value) => initHandlerTask2(data, doTask2, setTask2, value),
  handleInitTask3: (value) => initHandlerTask3(data, doTask3, setTask3, value),
});

export default AppViewHandler;
