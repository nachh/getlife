import axios from "axios";

export default {
  REQUESTS: [
    axios.get("https://jsonplaceholder.typicode.com/albums"),
    axios.get("https://jsonplaceholder.typicode.com/comments"),
    axios.get("https://jsonplaceholder.typicode.com/photos"),
    axios.get("https://jsonplaceholder.typicode.com/posts"),
    axios.get("https://jsonplaceholder.typicode.com/users"),
  ],

  CATEGORIES: ["albums", "comments", "photos", "posts", "users"],
};
