// @flow
// Vendors
import React from "react";
// Handlers
import AppViewHandler from "../handlers/app.view.handler";
// Utils
import { doTask1, doTask2, doTask3 } from "../utils/app.view.utils";
// Types
import type { viewHookReturnTypes } from "./types/app.view.hook.types";

const AppViewHook = (): viewHookReturnTypes => {
  const [data, setData] = React.useState(null);
  const [task1, setTask1] = React.useState([]);
  const [task2, setTask2] = React.useState([]);
  const [task3, setTask3] = React.useState([]);

  const {
    handleFetchAllData,
    handleInitTask1,
    handleInitTask2,
    handleInitTask3,
  } = AppViewHandler({
    data,
    doTask1,
    doTask2,
    doTask3,
    setData,
    setTask1,
    setTask2,
    setTask3,
  });

  React.useEffect((): void => {
    handleFetchAllData();
  }, []);
  React.useEffect((): void => {
    handleInitTask1();
    handleInitTask2();
    handleInitTask3();
  }, [data]);

  return {
    handleInitTask1,
    handleInitTask2,
    handleInitTask3,
    task1,
    task2,
    task3,
  };
};

export default AppViewHook;
