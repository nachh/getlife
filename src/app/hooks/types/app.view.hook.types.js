// @flow

export type viewHookReturnTypes = {
  handleInitTask1: Function,
  handleInitTask2: Function,
  handleInitTask3: Function,
  task1: Object,
  task2: Object,
  task3: Object,
};
