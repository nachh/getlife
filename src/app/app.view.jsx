// @flow
// Vendors
import * as React from "react";
// Hooks
import AppViewHook from "./hooks/app.view.hook";
// Components
import MasterTable from "./components/table/table.component";
// Constants
import constants from "./constants/app.view.constants";
// Styled
import {
  TableInputStyled,
  TableTitleStyled,
  TableWrapperStyled,
  WrapperStyled,
} from "./app.view.styled";

const AppView = (): React.Node => {
  const { handleInitTask2, handleInitTask3, task1, task2, task3 } =
    AppViewHook();

  return (
    <WrapperStyled>
      <TableWrapperStyled>
        <TableTitleStyled>Task #1</TableTitleStyled>
        <MasterTable data={task1} config={constants.TABLE1_PROPS} />
      </TableWrapperStyled>
      <TableWrapperStyled>
        <TableTitleStyled>Task #2</TableTitleStyled>
        <TableInputStyled
          max={100}
          min={0}
          placeholder="# last posts"
          onChange={(value) => {
            handleInitTask2(value);
          }}
        />
        <MasterTable data={task2} config={constants.TABLE2_PROPS} />
      </TableWrapperStyled>
      <TableWrapperStyled>
        <TableTitleStyled>Task #3</TableTitleStyled>
        <TableInputStyled
          placeholder="# last albums"
          type="text"
          onChange={(value) => {
            handleInitTask3(value);
          }}
        />
        <MasterTable data={task3} config={constants.TABLE3_PROPS} />
      </TableWrapperStyled>
    </WrapperStyled>
  );
};

export default AppView;
