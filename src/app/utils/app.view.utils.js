// @flow

const doTask1 = (data: Object): Object => {
  let task1 = [];
  data?.users.map((user) => {
    task1.push({
      name: user.name,
      email: user.email,
      nposts: data?.posts.filter((post) => post.userId === user.id).length,
    });
  });
  return task1;
};

const doTask2 = (data: Object, n: number): Object => {
  let task2 = [];
  data?.posts.slice(-n).map((post) => {
    task2.push({
      id: post.id,
      author: data?.users?.filter((user) => user.id === post.userId)[0]?.name,
      ncomments: data?.comments.filter((comment) => comment.postId === post.id)
        .length,
    });
  });
  return task2;
};

const doTask3 = (data: Object, n: number): Object => {
  let task3 = [];
  data?.albums.slice(-n).map((album) => {
    task3.push({
      id: album.id,
      author: data?.users?.filter((user) => user.id === album.userId)[0]?.name,
      nphotos: data?.photos.filter((photo) => photo.albumId === album.id)
        .length,
    });
  });
  return task3;
};

export { doTask1, doTask2, doTask3 };
