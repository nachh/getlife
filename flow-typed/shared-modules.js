// @flow

declare module "rsuite" {
  declare module.exports: any;
}
declare module "rsuite/dist/rsuite.min.css" {
  declare module.exports: any;
}
declare module "react-redux" {
  declare module.exports: any;
}
declare module "react-router-dom" {
  declare module.exports: any;
}
declare module "redux" {
  declare module.exports: any;
}
declare module "react-dom" {
  declare module.exports: any;
}
declare module "axios" {
  declare module.exports: any;
}
